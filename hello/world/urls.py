from django.urls import path
from .views import MonthlyProductSales

urlpatterns = [
    path(
        "monthly-product-sales/",
        MonthlyProductSales.as_view(),
        name="monthly-product-sales",
    ),
]