from django.shortcuts import render
from slick_reporting.views import ReportView, Chart
from slick_reporting.fields import SlickReportField
from .models import Sales
from django.db.models import Sum
from django.utils.translation import gettext_lazy as _

# Create your views here.


from slick_reporting.fields import SlickReportField


class SumValueComputationField(SlickReportField):
    computation_method = Sum
    computation_field = "value"
    verbose_name = _("Sales Value")
    name = "my_value_sum"



class MonthlyProductSales(ReportView):
    report_model = Sales
    date_field = "doc_date"
    group_by = "product"
    columns = ["name", "sku"]

    time_series_pattern = "monthly"
    time_series_columns = [
        SumValueComputationField,
    ]

    chart_settings = [
        Chart(
            _("Total Sales Monthly"),
            Chart.PIE,
            data_source=["my_value_sum"],
            title_source=["name"],
            plot_total=True,
        ),
    ]